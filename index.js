console.log("Hello World!");

let posts = [];
let count = 1;

document.querySelector("#form-add-post").addEventListener("submit", (event) => {
  event.preventDefault();
  posts.push({
    id: count,
    title: document.querySelector("#txt-title").value,
    body: document.querySelector("#txt-body").value,
  });
  count++;
  alert("Successfully added!");
  showPost(posts);
});

const showPost = (posts) => {
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `<div id="posts-${post.id}">
      <h3 id="post-title-${post.id}">${post.title}</h3>
      <p id="post-body-${post.id}">${post.body}</p>
      <button onclick=editPost(${post.id}) id="btn-edit-${post.id}">Edit</button>
      <button onclick=deletePost(${post.id}) id="btn-delete-${post.id}">Delete</button>
    </div>`;
  });
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
};

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();
  for (let i = 0; i < posts.length; i++) {
    if (
      posts[i].id.toString() === document.querySelector("#txt-edit-id").value
    ) {
      posts[i].title = document.querySelector("#txt-edit-title").value;
      posts[i].body = document.querySelector("#txt-edit-body").value;
      showPost(posts);
      alert("Successfully updated.");
      break;
    }
  }
});

const deletePost = (id) => {
  let formElement = document.querySelector(`#post-title-${id}`);
  formElement.parentElement.replaceChildren();
  let index = posts.findIndex((post) => post.id == id);
  posts.splice(index, 1);
};
